# M300 - Kubernetes LB2
- Datum: 01.07.2023
- Autor: Tim Schefer

Dieses Repository wurde aus der [Anleitung](https://gitlab.com/mbe99/kubernetes) zur erstellung eines 3 Node Kubernetes Clusters geforked, und beinhaltet somit sämtliche informationen zur LB2 in der K8s Variante.

## Struktur
Ausgehend vom originalen Repository wurde die Struktur leicht angepasst, neu gibt es folgende Unterordner:

1. **[Doku:](https://gitlab.com/m3003642022/lb-2-kubernetes/-/tree/master/doku)**
Hier findet man die gesamte Anleitung zum aufsetzen des Clusters, inklusive sämtlichen Ergänzungen und Verbesserungen meiner seite aus. Desweiteren werden alle Anpassungen dokumentiert die vorgenommen werden mussten um das Projekt auf meine Umgebung anzupassen.

2. **[Config:](https://gitlab.com/m3003642022/lb-2-kubernetes/-/tree/master/config)**
Im Config Verzeichnis befinden sich Dateien die ich zum Arbeiten brauche oder die mir das Arbeiten erleichtern, z.B. die SSH Config um den Zugriff auf die Maschinen zu vereinfachen, oder später eventuell eigene Servicedeklarationen.
