# Umgebung
## Infrastruktur
Eigentlich sieht die Anleitung vor dass das Kubernetes Cluster mit einem Typ 2 Hypervisor wie etwa Virtualbox oder VMWare Workstation direkt auf seinem Laptop virtualisiert, da relativ genaue Angaben zu Netzwerkkonfigurationen mit Adaptern etc. gemacht werden, in meinem Fall habe ich mich aber bewusst dagegen entschieden und die Virtuellen Maschinen welche als Grundlage für das Cluster dienen, auf einem, mit [Proxmox VE](https://www.proxmox.com/de/proxmox-ve) ausgerüsteten Server im Pizzaschachtel Format, der bei mir zuhause betrieben wird, laufen lassen. Vor ca. einem Jahr hatten wir im Rechenzentrum meines Arbeitgebers ein paar alte Systeme dekomissioniert, und ich hatte das privileg eines davon mitnehmen zu dürfen, da diese sonst sowieso im Elektroschrott gelandet werden. Der Server ist zwar nichtmehr der neuste, hat aber trotzdem noch mehr als genug Leistung, hier ein paar Spezifikationen:

System   | HP Proliant DL380 Gen 6 |
-------- | -------- |
End of Life Date | 30.04.2016 |
**Hardware**     |  |
CPU(s)           | 1x Intel Xeon X5670 (6C/12T @ 2.93 Ghz) |
RAM              | 8x 16GB DDR3 |
Disk             | 1TB 10K SAS HDD |


Das physische System sieht etwa so aus, für das initiale Setup wurde zwar noch Display und Tastatur angeschlossen, inzwischen läuft es aber Headless.
![Server](/doku/images/server.jpg)
 

Mir persönlich erleichtert dieses Setup das Arbeiten enorm, besonders froh bin ich über folgende Vorteile:
- Standordunabhängiger Zugriff auf K8s Cluster
- Ohnehin schon knappe Ressourcen von Laptop werden geschont
- Zentrale Verwaltung über Proxmox Webinterface

## Systemübersicht
Wie man erkennen kann wird für einen externen Zugriff auf die Virtuelle Umgebung eine [VPN-Verbindung](https://gitlab.com/m9065/doku/-/blob/main/Eigene%20Ideen/VPN.md) benötigt. Mit dieser kommt man dann übers Internet in mein Heimnetzwerk und kann direkt auf das Proxmox PVE Webinterface, den Hypervisor oder die Cluster VM’s zugreifen.
![Systemübersicht](/doku/images/zugriff_systemuebersicht.jpg)

## Proxmox Netzwerk
Die Anleitung sieht vor dass man jeder Virtuellen Maschine 2 Netzwerkadapter gibt, einmal NAT für Internetzugriff und einmal einen Host Only im 192.168.11.0/24er Netz für die interne Kommunikation zwischen den Cluster membern.
Für Host Only hat Proxmox noch keine vorgefertigen Adapter, dieser hätte aufgrund der Netzwerkrange allerdings sowieso neu von Hand erstellt werden müssen. Die Konfiguration dazu findet man bei Proxmox unter `/etc/network/interfaces` ein neuer Adapter, kann wie folgt hinzugefügt werden

```less
iface vmbr1 inet static
        address 192.168.11.0/24
        bridge-ports none
        bridge-stp off
        bridge-fd 0
#Host Only
```

Wenn man diese Datei jetzt speichert braucht man das networking mit `systemctl restart networking` nurnoch kurz neuzustarten, und schon wird der Adapter im Web UI erkannt, und kann ausgewählt werden.

Somit waren grundsätzlich alle Vorbereitungen getroffen, und ich habe damit begonnen, der Anleitung Schritt für Schritt zu folgen, sämtliche Anpassungen/Ergänzungen die ich dort noch gefunden habe, werden direkt in den entsprechenden Markdown Files vorgenommen.
