# Auftrag
Grundsätzlich gibt es im Modul 300 zwei verschiedene Leistungsbeurteilungen, die LB1 wird theoretischer natur sein, und primär mit Multiple Choice fragen auf Ecolm stattfinden, während die LB2 ein praktisches Projekt ist.
Für dieses Projekt gab es 2 verschiedene Varianten zwischen denen wir uns entscheiden durften, unabhängig davon dürfen wir zusammen in Gruppen arbeiten, die abgabe erfolgt am 07.07.2023 allerdings einzeln.

## Variante 1 - Container (regulär)
Bei der ersten Variante besteht der [Auftrag](https://tbzedu.sharepoint.com/sites/M300_Documents/Freigegebene%20Dokumente/Forms/AllItems.aspx?id=%2Fsites%2FM300%5FDocuments%2FFreigegebene%20Dokumente%2FLeistungsbeurteilung%2FM300%5FLB2%5FContainer%5FST20d%5F1%5F3%2Epdf&parent=%2Fsites%2FM300%5FDocuments%2FFreigegebene%20Dokumente%2FLeistungsbeurteilung&p=true&ga=1) der LB2 einfach gesagt darin sich selbst eine Hand voll Services auszusuchen, diese über mindestens ein docker-compose File containerisiert zu deployen, und das entsprechende Vorgehen in Markdown zu dokumentieren.
Für die Bewertung gibt es hier ein ganz klares [Raster](https://tbzedu.sharepoint.com/sites/M300_Documents/Freigegebene%20Dokumente/Forms/AllItems.aspx?id=%2Fsites%2FM300%5FDocuments%2FFreigegebene%20Dokumente%2FLeistungsbeurteilung%2FBewertung%20LB2%5F1%5F3%2Epdf&parent=%2Fsites%2FM300%5FDocuments%2FFreigegebene%20Dokumente%2FLeistungsbeurteilung&p=true&ga=1) mit eindeutigen Kriterien, von denen für eine bestimmte Note immer eine bestimmte Mindestanzahl erfüllt sein müssen, man kann also relativ genau abschätzen wieviel man noch Investieren muss um das gewünschte Resultat zu erreichen, vom Aufwand/Ertragsverhältnis sicher die lukrativere Option, zumindest wenn es einem nur um die Note geht. 

## Variante 2 - Kubernetes (Option)
Wenn man hingegen ein bisschen eine challange sucht, dann ist die zweite Variante vielleicht eher etwas für einem, hier geht es darum anhand einer vorgefertigten Anleitung ein K8s Cluster bestehend aus einer Master und zwei Worker Nodes zum laufen zu bringen, wer das schafft bekommt automatisch die Note 5, um besser abzuschneiden muss noch mehr Eigenleistung, z.B. in Form von zusätzlich deployten Services, oder verbesserungen die man an der bestehenden Dokumentation vorgenommen hat erbracht werden. Für den Kubernetes Task gibt es kein Bewertungsraster, hier wird individuell geschaut wieviel man erreicht hat, somit geht man zwar ein grösseres Risiko ein keine 6 zu bekommen, kommt dafür aber noch mit Kubernetes in Berührung, und lernt so etwas dazu was man sonst nicht gelernt hätte, wenn einem also wichtiger ist, möglichst viel neues Wissen zu gewinnen, dann ist dies vielleicht die bessere Wahl.

# Kubernetes
## Was ist Kubernetes ?

Kubernetes ist eine portable, erweiterbare Open-Source-Plattform die von Goole entwickelt und im 2014 öffentlich gemacht wurde. 

Die K8s Platform (Abkürzung für Kubernetes) dient zur Verwaltung containerisierter Arbeitslasten und Dienste, die sowohl die deklarative Konfiguration als auch die Automatisierung erleichtert. Der Name kommt von alten Griechischen und bedeutet so viel wie Pilot.

![ContainerEvolution](/doku/images/container_evolution.png)

### Wie ist K8s aufgebaut ?

Auf folgendem Bild ist die Grundlegende Kubernetes Architektur zu sehen:

<img src="/doku/images/k8s_architektur.png"  alt="arch" width="70%">

Jedes Cluster setzt sich in der Basis aus dem abgebildeten Komponenten zusammen, man hat immer mindestens einen Master oder *Control Plane*, und mindestens einen *Worker Node*, wobei man diese theoretisch beliebig skalieren, und die Ressourcendimensionierung so ganz seinen Bedürfnissen anpassen kann.
Master und Worker Nodes werden durch ein sogenanntes *virtuelles Overlay Netzwerk* miteinander Verbunden, dieses ist mit eines der Wichtigsten Komponenten überhaupt, da es den einzelnen Cluster Membern überhaupt erst erlaubt untereinander zu kommunizieren, erst so können die einzelnen Nodes zu dieser potentiell riesigen logischen Maschine mit der Summe all ihrer Rechenleistung werden.

Master und Worker Nodes unterscheiden sich im Normalfall wie folgt:

**Master Nodes / Control Plane:**
- Beherbergt nur eine Handvoll Masterprozesse, darauf werden keine Applikationen deployed
- Master Node ist wichtiger als Worker Node, wenn man nur 1 Master hat und dieser ausfällt ist ganzes Cluster tot, bei einzelner Worker Node weniger tragisch
- Weniger Ressourcen, dafür gut abgesichert durch redundanz etc.


**Worker Nodes**
- Führ höhere Workloads ausgelegt, darauf laufen später alle Pods mit Applikationen
- Viel grösser Dimensioniert, braucht keine besonderen Sicherheitsvorkehrungen

#### Kubernetes Komponenten
Auf der eben beschriebenen Grundlage bietet Kubernetes jetzt verschiedene Komponenten an mit denen gearbeitet werden kann, diese wären:
- Pod: abstraktion von Container
- Service: Kommunikation
- Ingress: Traffic in Cluster Routen
- ConfigMap: Externe Konfigurationen
- Secret: geheime externe Konfiguration
- Volumes: Datenpersistenz
- Deployment: Replikation
- StatefulSet: Replikation(Stateful)

Um die Komponenten besser zu verstehen, möchte ich kurz auf jede einzelne eingehen, und anhand eines kleines Beispiels veranschaulichen was sie macht.

##### Pod
Pods sind die schmalste einheit in Kubernetes, ein Pod ist im Prinzip eine Abstraktion eines Docker Containers, sozusagen ein zusätzlicher wrapper wenn man so möchte, der von Kubernetes um den Container gespannt wird, damit der Container Orchestriert werden kann, und es nur den Kubernetes Layer gibt, auf dem man mit den Pods bzw. den Containern dahinter interagiert.

<img src="/doku/images/pod.png"  alt="statefulset" width="80%">

Normalerweise beinhaltet jeder Pod nur einen einzigen Container, und somit eine einzige Applikation, wenn ich also z.B. eine Webapplikation mit Datenbank habe, dann wären diese beiden jeweils in getrennten Pods untergebracht. Damit diese Pods jetzt untereinander kommunizieren können bekommen sie eigene interne IP Adressen zugewiesen, da Pods allerdings flüchtig sind, und jedes mal eine andere IP bekommen wenn sie neustarten, macht es nur wenig Sinn irgendwo direkt über die IP zu komunizieren, um dieses Problem zu lösen gibt es *Services*. 

##### Service & Ingress
Ein Service ist sozusagen eine statische IP-Adresse die man einem Pod anhängen kann, wenn ich für meine Webapplikation und meine Datenbank also jeweils eigene Services erstelle ist das oben beschriebene Problem erstmal behoben. Das gute daran ist dass der Lifecycle von Pods und Services nicht verbunden ist, der Service kann weiterbestehen auch wenn ein Pod neustartet oder beendet wird.

Services können vom Typ entweder *Intern* oder *Extern* eingestellt werden, bei der Webapp und der dazugehörigen DB will man natürlich über den Browser auf die App, aber nicht auf die Datenbank zugreifen können, diese umstände sollte man Berücksichtigen.
<img src="/doku/images/service.png"  alt="service" width="70%">

Was auf dem Bild ebenfalls auffällt ist das aktuell zwar über durch die Services persistierte IP-Adressen zugegriffen werden kann, der Fakt dass man sich eine lange zahlenabfolge merken muss hat sich allerdings immernoch nicht geändert, es wäre doch schön wenn man nicht nur über IP's sondern auch über Hostnamen auf Applikationen zugreifen könnte.
Hier kommen Ingresses ins spiel, ein Ingress ist sozusagen ein DNS Server für ein K8s Service, alle Requests die an die Webapp gehen, landen beim Setup mit Ingress zuerst bei diesem, und werden dann erst an den Service, respektive den Pod weitergeleitet.

##### ConfigMap & Secret
Wenn wir jetzt wieder unsere Webapp und unsere Datenbank nehmen, und daran einen Konfigurationswert, z.B. den Database Endpoint verändern wollen, dann würde man das normalerweise wahrscheinlich in irgendeinem Properties File, über eine Environment variable, oder etwas ähnliches machen. Das Problem was allerdings entsteht wenn man mit Containerisierten Umgebungen arbeitet, ist dass all diese Parameter höchstwahrscheinlich teil eines fertig gebauten (Docker-)Images sind, und im nachhinein somit nichtmehr angepasst werden können, es müsste also für jede änderung auch ein neues Image gebaut werden.
Da das allerdings sehr umständlich wäre gibt es ConfigMaps, ConfigMaps erlauben einem alle diese Applikationsspezifischen Konfigurationen, wie eben z.B. den DB Endpoint, in einer externen Datei zu pflegen, die man dann an seine Pods anhängen kann. Ab hier müssen alle änderungen nichtmehr im Image, sondern nurnoch in der Configmap vorgenommen werden.

Teile dieser Externen Konfiguration, wie etwa Passwörter oder Usernames, können unter umständen Confidential sein, und man will sie nicht unbedingt im Plaintext eintragen, so dass sie von jedermann gelesen werden könnten, zu diesem zweck hat K8s Secrets geschaffen. Secrets werden wie gesagt dazu verwendet geheime Daten zu speichern, alles was als Secret deklariert wird, wird Base64 encoded, so sind die Strings zwar für Menschen nichtmehr lesbar, mit entsprechenden [Tools](https://www.base64decode.org/) können die Werte dahinter aber ganz leicht herausgefunden werden, für eine tatsächliche Verschlüsselung hat Kubernetes also keine Lösung, wenn man dies möchte müsste man auf ein drittes Tool zurückgreifen.

<img src="/doku/images/configmap_secret.png"  alt="configmap_secret" width="60%">

##### Volume
Wie bereits erwähnt sind Container flüchtig, die Daten der Datenbank würden also z.B. jedes mal verloren gehen, wenn der entsprechende Pod neugestartet oder beendet wird.
Damit die Daten persistent gemacht werden können gibt es Volumes, mit einem Volume hängt man einen Physischen Speicherort, entweder auf der Lokalen Festplatte, oder auf einem Netzwerkshare, an den Pod von welchem man die Daten speichern möchte, auf dieses Filesystem wird er dann alles relevante ablegen.
Der Storage hat nichts mit dem Cluster direkt zu tun, man kann ihn sich wie eine externe Festplatte vorstellen, die an das ganze System angeschlossen wird.
<img src="/doku/images/volumes.png"  alt="volumes" width="60%">

##### Deployment & StatefulSet
Nehmen wir jetzt mal an dass alles Perfekt läuft, und man die Webapp als Benutzer über einen Ingress Hostnamen im Browser aufrufen kann. Wenn jetzt der Pod mit der Webapp abstürzen würde, würde das zu einer gewissen Downtime führen, da man das in einer Produktiven Umgebung natürlich nicht will, hat man im normalfall immer mehrere Kopien, sogenannte *replicas* eines bestimmten Pods. Wieviele dieser Replicas es geben soll ist einer von vielen Einstellungen die in einem *deployment* vorgenommen werden können, ein Deployment ist sozusagen ein Blueprint wie der Pod später aussehen soll, man kann ihn sich ähnlich wie ein [docker-compose](https://docs.docker.com/compose/) file vorstellen.

In der Praxis arbeitet man nicht direkt mit Pods, sondern erstellt Deployments in Form von YAML Files, die man später anwendet um zu definieren wie der/die Pods, welche man deployen, daher auch der name, will, aussehen sollen. Wir haben ja gesagt das Pods bereits eine weitere Abstraktionsschicht über Container sind, mit Deployments kommt jetzt nochmal ein Layer über den Pods hinzu, die hirarchie sieht etwa so aus:
- Deployment
  - Pod
    - Container

Wenn jetzt aber nicht der Webapp Pod, sondern derjenige mit der Datenbank ausfallen würde, dann hätte man wieder ein Problem, Datenbanken können nämlich nicht über Deployments repliziert werden, da sie stateful sind, und Daten beherbergen. Wenn man Replicas der Datenbank hätte müssten diese alle auf denselben geteilten Storage zugreifen, und es bräuchte irgendeinen Mechanismus der Überprüft wer gerade auf diesen Storage schreibt und wer davon liest um Dateninkonsistenz zu vermeiden. Dieser Mechanismus, zusammen mit der replikation wird von K8s angeboten, das ganze nennt sich *Statefulset*.
Diese komponente ist eine ergänzung zu Deployments und speziell für Datenbanken oder andere Applikationen gedacht die eine State haben, und wo Daten synchronisiert werden müssen.

<img src="/doku/images/stateful_set.png"  alt="statefulset" width="60%">

Stateful sets einzurichten ist allerdings nicht ganz so einfach wie mit Deployments zu arbeiten, desshalb wird auch oft auf externe Datenbanken zurückgegriffen die ausserhalb des Clusters gehostet werden, und die Stateless Applikationen welche im CLuster ohne probleme replizieren und skalieren lässt man dann damit kommunizieren.

#### Kubernetes Configuration
Wie wir jetzt also wissen werden K8s Konfigurationen in YAML Files geschrieben, ein solches könnte z.B. so ausssehen:

```yaml
# nginx-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-development
  labels: ...
spec:
  replicas: 2
  selector: ...
  template: ...
```

Jede Kubernetes config besteht im wesentlichen aus 3 verschiedenen Teilen:
1. Metadata
2. Specification
3. Status

Die ersten beiden Teile sind bereits im File ersichtlich unter den *Metadaten* sagt man- einfach, was für eine Datei dass gerade erstellt werden soll, im Beispiel ist das ein Deployment.
Je nachdem was hier für ein Typ gewählt wird hat man im zweiten Abschnitt den *Spezifikationen* andere Parameter zur auswahl, ich kann für ein Deployment andere Einstellungen vornehmen als z.B. für einen Service. 

Der dritte Bestandteil der Konfiguration, der *status* ist nicht im File ersichtlich, darum kümmert sich kubernetes ständig von alleine. Die YAML Files welche wir schreiben sind Deklarativ, bedeutet dass man immer sagt welchen zustand eine Bestimmte Ressource haben soll, wie sie diesen erreicht muss man dabei nicht konkreter spezifizieren. K8s gleicht den deklarierten State ständig mit dem tatsächlichen in einer Key Value Datenbank ab, und setzt den tatsächlichen Status wenn nötig auf den gewünschten, so dass die Konfiguration auch effektiv angewandt wird.

## Quellen
- https://www.youtube.com/watch?v=s_o8dwzRlu4