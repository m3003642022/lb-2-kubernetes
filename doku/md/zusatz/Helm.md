# Idee
Um nochmal etwas mehr eigene Erfahrung mit Kubernetes sammeln zu können hatte ich die Idee mithilfe von [Prometheus](https://prometheus.io/) ein Monitoring aufzusetzen, was das gesamte Cluster überwacht, und die Daten in einem [Grafana](https://grafana.com/) Dashboard visualisiert.
Wenn alles so funktioniert wie ich mir das vorstelle, dann würde die Architektur schlussendlich etwa so aussehen:

<img src="/doku/images/monitoring.png"  alt="monitoring" width="70%">

# Helm
Um Prometheus und Grafana jetzt zu installieren und Konfigurieren gäbe es mehrere möglichkeiten:

**1. Alle YAML Files selber erstellen:**
Man könnte natürlich die kompletten Deployments, Statefullsets, Configmaps, Secrets, die Prometheus und Grafana brauchen, um richtig funktionieren zu können von Hand erstellen, das wäre allerdings sehr mühsam, da die Applikationen erstens relativ Komplex sind, und man alle Dateien auch in der richtigen Reihenfolge anwenden müsste, da gewisse Abhängigkeiten untereinander bestehen würden.
Diese Variante wäre sehr Ineffizient und Aufwendig, man müsste sehr gut darüber bescheid wissen was man da gerade tut, in der Praxis wird sich in so einem szenario wahrscheinlich niemand dafür entscheiden.

**2. Helm:**
Die andere möglichkeit die man hätte wäre Helm zu verwenden. Helm ist sozusagen ein Package Manager für Kubernetes, und hilft einem somit dabei die Umständlichkeiten zu umgehen, mit denen man bei Variante 1 unweigerlich konfrontiert wäre, Helm paketiert alle verschiedenen YAML Files die man benötigt in ein sogenanntes *Helm Chart* Repository, von wo aus man das initiale setup für sein Projekt dann ganz entspannt starten kann.

Ich habe mich aus den eben genannten Gründen für Variante 2 entschieden, und werde dementsprechend mit Helm arbeiten.

[zurück](README.md) |  [Home](../../README.md)