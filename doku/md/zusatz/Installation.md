# Installation von Prometheus und Grafana

## Helm
Bevor wir Helm verwenden können müssen wir dieses zuerst installieren, standardmässig ist es noch nicht auf dem System vorhanden. Das können wir wie folgt tun:
```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

Sobald helm installiert ist müssen wir die entsprechenden Repositories für unser vorhaben hinzufügen:
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
helm repo update
```

## Monitoring Stack

### Installation

Wenn dann alle benötigten Repos auf dem aktuellsten stand sind, kann der Monitoring stack folgendermassen installiert werden:
```bash
helm install prometheus prometheus-community/kube-prometheus-stack
```

Je nachdem dauert die installation jetzt eine ganze weile, es muss schliesslich auch einiges an software geladen werden. Wenn wir uns mit `kubectl --namespace default get all -l "release=prometheus"` nur mal alle Komponenten anschauen die fürs Monitoring relevant sind, sieht man das es doch eine ganze menge geworden sind:
```less
NAME                                                         READY   STATUS    RESTARTS      AGE
pod/alertmanager-prometheus-kube-prometheus-alertmanager-0   2/2     Running   0             15m
pod/prometheus-grafana-5b55f9d866-9h8bv                      3/3     Running   0             18m
pod/prometheus-kube-prometheus-operator-7c88b9576b-rbppt     1/1     Running   0             18m
pod/prometheus-kube-state-metrics-5db49ccbb8-9sc2v           1/1     Running   0             18m
pod/prometheus-prometheus-kube-prometheus-prometheus-0       2/2     Running   0             15m
pod/prometheus-prometheus-node-exporter-22fr5                1/1     Running   0             18m
pod/prometheus-prometheus-node-exporter-4w8xn                1/1     Running   0             18m
pod/prometheus-prometheus-node-exporter-d6xsq                1/1     Running   0             18m

NAME                                              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
service/alertmanager-operated                     ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   16m
service/kubernetes                                ClusterIP   10.96.0.1        <none>        443/TCP                      4d
service/prometheus-grafana                        ClusterIP   10.102.173.58    <none>        80/TCP                       18m
service/prometheus-kube-prometheus-alertmanager   ClusterIP   10.110.85.92     <none>        9093/TCP,8080/TCP            18m
service/prometheus-kube-prometheus-operator       ClusterIP   10.108.229.255   <none>        443/TCP                      18m
service/prometheus-kube-prometheus-prometheus     ClusterIP   10.104.187.36    <none>        9090/TCP,8080/TCP            18m
service/prometheus-kube-state-metrics             ClusterIP   10.109.89.143    <none>        8080/TCP                     18m
service/prometheus-operated                       ClusterIP   None             <none>        9090/TCP                     16m
service/prometheus-prometheus-node-exporter       ClusterIP   10.111.121.158   <none>        9100/TCP                     18m

NAME                                                 DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/prometheus-prometheus-node-exporter   3         3         3       3            3           kubernetes.io/os=linux   18m

NAME                                                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/prometheus-grafana                    1/1     1            1           18m
deployment.apps/prometheus-kube-prometheus-operator   1/1     1            1           18m
deployment.apps/prometheus-kube-state-metrics         1/1     1            1           18m

NAME                                                             DESIRED   CURRENT   READY   AGE
replicaset.apps/prometheus-grafana-5b55f9d866                    1         1         1       18m
replicaset.apps/prometheus-kube-prometheus-operator-7c88b9576b   1         1         1       18m
replicaset.apps/prometheus-kube-state-metrics-5db49ccbb8         1         1         1       18m

NAME                                                                    READY   AGE
statefulset.apps/alertmanager-prometheus-kube-prometheus-alertmanager   1/1     16m
statefulset.apps/prometheus-prometheus-kube-prometheus-prometheus       1/1     16m
```

Wie man sieht werden hier die verschiedensten [Komponenten](/doku/md/vorwort/auftrag.md) verwendet, jeder Eintrag in dieser Liste hätte man sonst von Hand in eine YAML Datei schreiben, über Helm geht die installation davon aber völlig automatisch.
Es werden einmal 2 Stateful Sets, für Prometheus selbst, und den alertmanager, ein anderer bestandteil des Stacks erstellt, daneben gibt es die entsprechenden Deployments, welche einmal den Prometheus Operator, Grafana, und Kube State Metrics bereitstellen. Letzteres sammelt automatisch K8s Component Metrics, überwacht also den Zustand des Clusters und bereitet diesen für Prometheus bzw. Grafana, vor, für uns ist das besonders cool, da wir so out of the box das gewünschte Infrastruktur Monitoring bekommen, und nichts spezielles mehr konfigurieren müssen.
Sonst gibt es noch die ganzen replicasets welche von den Deployments erstellt werden, und eine weitere interessante Komponente, das daemonset. Ein daemonset läuft auf jeder Workernode des Clusters, in diesem Fall, holt es sich die Worker Node Metrics, wie CPU auslastung usw. und wandelt diese in Prometheus Metrics um, damit sie vom Stack interpretiert werden können.

So wurde zwar alles schön für einem aufgesetzt, was allerdings auch den Nachteil haben kann dass gewisse Konfigurationen schwerer nachzuvollziehen sind, als wenn man sie selbst geschrieben hätte. Um das trotzdem, wenigstens ein Stückweit zu können, kann man sich die descriptions der deployments anzeigen lassen, und bei bedarf sogar in einer Datei abspeichern. Dann kann Zeile für zeile nachgeschaut werden, was denn im Hintergrund überhaupt passiert:
```bash
kubectl describe deployment prometheus-kube-prometheus-operator > oper.yaml
```

### Zugriff Grafana
Wenn wir `kubectl get service` ausführen, sehen wir dass der Grafana Service vom Type Cluster IP ist, Grafana ist also stand jetzt erst von Intern erreichbar. In der praxis würde man jetzt einen Ingress einrichten, für mich recht es aber wenn Port forwarding konfiguriert wird.
```bash
# kubectl get service
NAME                                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
prometheus-grafana                        ClusterIP   10.102.173.58    <none>        80/TCP                       37m
```

Um sehen zu können welcher Port weitergeleitet werden soll kann man sich die logs vom Pod entsprechenden Pod anzeigen lassen, dort wird man sehen auf welchem dieser zuhört:
```bash
kubectl get pod
kubectl log prometheus-grafana-5b55f9d866-9h8bv
```

Im Output ist zu erkennen dass Grafana auf *Port 3000* hört, wir richten also folgendes Port Forwarding ein:
```bash
kubectl expose deployment/prometheus-grafana --type=NodePort --port=3000 --target-port=3000 --name=grafana-dashboard
kubectl get service | grep grafana-dashboard
```

Über den Port der einem jetzt angezeigt wird, kann man dann erstmals auf grafana zugreifen, die Standarmässigen Credentials sind im Bild ersichtlich:

<img src="/doku/images/grafana_login.png"  alt="grafana_login" width="60%">


In Grafana angekommen kann man dann unter `Home -> Dashboard` die übersicht aller Standardmässigen Dashboards abrufen, und sich eines seiner Wahl anzeigen lassen. Es gibt Dashboards für das Gesamte cluster, für einzelne Pods, Deployments, usw. Die Auswahlmöglichkeiten hören nicht auf, und das obwohl diese Vorgefertigten Dashboard erst die Spitze des eisbergs sind, man könnte theretisch auch noch eigene Prometheus abfragen erstellen, und für diese dann eigene Dashbaords bauen, das würde allerdings den ramen sprechen, deswegen belassen wir es hierbei, und erfreuen uns daran wie gut das Cluster läuft:

<img src="/doku/images/grafana_dashboard.png"  alt="grafana_login" width="70%">




# Quellen
- https://www.youtube.com/watch?v=QoDqxm7ybLc
- https://www.youtube.com/watch?v=-ykwb1d0DXU
- https://helm.sh/docs/intro/install/
- https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack

[zurück](README.md) |  [Home](../../README.md)