# Kubernetes Dashboard

1. [Self-Signed Zertifikat erstellen](certificate.md)
2. [Kubernetes Dashboard Installation](dashboard.md)

[Home](../../README.md)
